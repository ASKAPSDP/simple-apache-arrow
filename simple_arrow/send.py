"""
    
    This purpose of this is not to produce some nice extensible application.
    Instead all I want the do is send an array object to a plasma store and retrieve it

    So expect some hard coded bits and bobs

    
"""
import pyarrow.plasma as plasma

"""
    I'm just going to define the object here - so I dont need to replicate between
    send and recv
"""

import simple_arrow.object as obj
import sys
import numpy as np    

def main():
    """ Remember before this you have to start up the plasma store using something like:
        plasma_store -m 1000000000 -s /tmp/plasma
    """
    try:
        client = plasma.connect("/tmp/plasma")
    except:
        print ("Oops ", sys.exc_info()[0], "occurred. Is the plasma store running")
        return

    # Create a object.
    my_object = obj.object()
    # need a 20 byte ID - I'm cheating here - making a ljust 20 character string from my ID
    my_str = my_object.id_str.ljust(20,'x')
    oid_as_bytes = str.encode(my_str)
    
    # Get a true ID from my byte string
    object_id = plasma.ObjectID(oid_as_bytes)

    # Size of the data block
    object_size = my_object.data.nbytes
    
    # defines a memory buffer for the client
    buffer = memoryview(client.create(object_id, object_size))

    # We need to copy into it

    # Write to the buffer.
    for i in range(my_object.data.size):
        buffer[i] = my_object.data[i]

    # need to seal it so it is available to something else

    client.seal(object_id)
 
if __name__ == '__main__':
    main()    
