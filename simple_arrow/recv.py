"""
    
    This purpose of this is not to produce some nice extensible application.
    Instead all I want the do is send an array object to a plasma store and retrieve it

    So expect some hard coded bits and bobs

    
"""
import pyarrow.plasma as plasma
import pyarrow as pa
"""
    I'm just going to define the object here - so I dont need to replicate between
    send and recv
"""

import simple_arrow.object as obj
import sys
import numpy as np    

def main():
    """ Remember before this you have to start up the plasma store using something like:
        plasma_store -m 1000000000 -s /tmp/plasma
    """
    try:
        client = plasma.connect("/tmp/plasma")
    except:
        print ("Oops ", sys.exc_info()[0], "occurred. Is the plasma store running")
        return

    # Create a object.
    my_object = obj.object()
    # need a 20 byte ID - I'm cheating here - making a ljust 20 character string from my ID
    my_str = my_object.id_str.ljust(20,'x')
    oid_as_bytes = str.encode(my_str)
    
    # Get a true ID from my byte string
    object_id = plasma.ObjectID(oid_as_bytes)

    # Lets get it

    [buffer] = client.get_buffers([object_id])

    # Lets test it there is a numpy integration

    view = memoryview(buffer)
    
    # test
    i = 0
    for x in my_object.data:
        print(view[i],x)
        i = i + 1

    client.delete([object_id])

if __name__ == '__main__':
    main()    
