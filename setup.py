#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='simple-apache-arrow',
    version='0.1',
    description="",
    long_description=readme + '\n\n',
    author="Stephen Ord",
    author_email='stephen.ord@csiro.au',
    url='https://github.com/steve-ord/simple-apache-arrow',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['send-array=simple_arrow.send:main','recv-array=simple_arrow.recv:main']
    },
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=[
        "numpy",
        "pyarrow",
        "sh"
    ],
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
    ]
    )
