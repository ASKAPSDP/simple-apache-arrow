# Simple Apache Arrow

A really simple Apache Arrow enabled application that just writes a Numpy array to a plasma store, then reads from the store and compares the arrays. Written purely for self-education purposes as I cannot find a simple example on-line.